# pgr - alternate php build tool -

## requirements

phpenv

## install

````
git clone https://github.com/chobie/pgr.git
cd pgr
./pgr install 5.4.0-zts
````

## commands

````
pgr install <manifest> [configure_options]	install php with specified manifest.
pgr known					show manifests list.
````